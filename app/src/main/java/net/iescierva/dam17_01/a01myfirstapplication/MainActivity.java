package net.iescierva.dam17_01.a01myfirstapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button calcularBtn;
    EditText val1, val2;
    TextView resultado;
    public static double valResul = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        calcularBtn = (Button) findViewById(R.id.calcularBtn);
        val1 = (EditText) findViewById(R.id.valor1Et);
        val2 = (EditText) findViewById(R.id.valor2Et);
        resultado = (TextView) findViewById(R.id.resultadoTv);


        calcularBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    valResul = Double.parseDouble(val1.getText().toString()) + Double.parseDouble(val2.getText().toString()) ;
                    resultado.setText(Double.toString(valResul));
                    val1.setText("");
                    val2.setText("");
                    val1.requestFocus();

                } catch (Exception e){
                    resultado.setText("Datos erroneos");
                    val1.setText("");
                    val2.setText("");
                    val1.requestFocus();

                }
            }
        });


    }
}
